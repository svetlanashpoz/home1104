﻿using HOME_1104.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace HOME_1104.Controllers
{
	public class HomeController : Controller
	{
		public IActionResult Index()
		{
			return View();
		}
		[HttpGet]
		public ViewResult SponsorForm()
		{
			return View();
		}
		[HttpPost]
		public ViewResult SponsorForm([FromForm]Sponsors sponsors)
		{
			return View("Thanks", sponsors);
		}
	}
	
}
